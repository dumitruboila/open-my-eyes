<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Open My Eyes</title>
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <meta name="expires" content="tue, 01 Jun 2010 19:45:00 GMT">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
</head>
<body>
        <div id="container">
            <h1>Hello, OpenMyEyes!</h1>
            <form method="post" enctype="multipart/form-data" action="upload.php" id="uploadForm">
                <div class="camera">
                    <input type="file" accept="image/*" capture="camera" name="picture" id="picture"/>
                </div>
                <?php $image = !empty($_GET['image']) ? $_GET['image'] : ''; ?>
                <input type="hidden" id="uploadedPicture" name="uploaded_picture" value="<?php echo $image; ?>">
            </form>
        </div>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://code.responsivevoice.org/responsivevoice.js"></script>
    <script src="js/app.js"></script>
</body>
</html>
