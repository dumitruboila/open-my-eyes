<?php

class UploadFile
{
    /** @var string  */
    private $serverURL = 'http://188.166.98.14/open-my-eyes/';

    /** @var string  */
    private $uploadPath = 'uploads/';

    public function upload()
    {
        if (empty($_FILES['picture'])) {
            throw new \InvalidArgumentException("Please provide a valid image in order to be uploaded");
        }

        try {
            $path = $this->uploadPath . basename($_FILES['picture']['name']);

            if (move_uploaded_file($_FILES['picture']['tmp_name'], $path)) {
                $this->returnResourceUrl($path);
            }
        } catch (\Exception $e) {
            throw new \Exception("Captured picture could not be uploaded to server: " . $e->getMessage());
        }

    }

    private function returnResourceUrl($filePath)
    {
        $location = sprintf("Location: index.php?image=%s", $this->serverURL . $filePath);
        header($location);
    }
}

try {
    $uploader = new UploadFile();
    $uploader->upload();
} catch (\Exception $e) {
    throw $e;
}
