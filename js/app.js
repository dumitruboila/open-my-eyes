$(document).ready(function(){
    var uploadedPicture = $('#uploadedPicture').val();

    if (uploadedPicture.trim() != "") {
        callAPI(uploadedPicture);
        $('#uploadedPicture').val('');
    }

    $('#picture').change(function () {
        $('#uploadForm').submit();
        responsiveVoice.speak("Wait, while I figure out what's in front of you.");
    });
});

function callAPI(picture) {
    var xhttp = new XMLHttpRequest();
    var imageAPI = 'https://westcentralus.api.cognitive.microsoft.com/vision/v2.0/analyze';
    var apiKEY = '3f940bf7e26b4f5a8b87c6ce3b2f9fb2';
    var params = "visualFeatures=Description&details=&language=en";

    xhttp.open("POST", imageAPI + "?" + params);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader("Ocp-Apim-Subscription-Key", apiKEY);
    xhttp.send("{'url':'" + picture + "'}");

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            console.log("Response: " + xhttp.responseText);
            response = JSON.parse(xhttp.responseText);
            playVoice("Your picture is about: " + response.description.captions[0].text);
        }
    }
}

function playVoice(message)
{
    responsiveVoice.speak(message, null, {
        onstart: function () {
            console.log("start to talk ");
        }, onend: function () {
            window.location = window.location.href.split("?")[0];
        }
    });
}